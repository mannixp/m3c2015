!statsmod2.f90
!module containing routines to compute the mean and variance of an array
module stats
    implicit none
    integer :: Nstats !size of array (set in calling program)
    save
contains

function compute_mean(f)
!Compute mean using f and N
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8) :: compute_mean

    compute_mean = sum(f)/dble(Nstats)

end function compute_mean

subroutine compute_var(f,fmean,fvar)
!Compute variance using f,N, and input variable fmean
!Return the variance via the output variable, fvar
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), intent(in) :: fmean
    real(kind=8), intent(out) :: fvar

    fvar = sum((f-fmean)**2)/dble(Nstats)

end subroutine compute_var

end module stats

