!Module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test functions apply these methods to a Gaussian function and
!return performance information.

module fdmodule
 implicit none
 integer :: N !1000
 !integer :: a
 double precision :: dx
 !dx = 2.0d0/(N-1) 
 !double precision, parameter :: dx = 2.0d0/(N-1) 
 !save

contains
!-------------------
subroutine fd2(f,df)
 implicit none
 !2nd order centered finite difference
 !Header variables
 !f: function to be differentiated
 !df: derivative of f
 integer :: i1
 real(kind=8), dimension(:), intent(in) :: f
 real(kind=8), dimension(size(f)-2), intent(out) :: df
	
 do i1 = 1,N-2
  df(i1) = ( f(i1+1) - f(i1-1) )*(1/(2.0d0*dx))
 end do
	
end subroutine fd2
!-----------------

subroutine cfd4(f,df)
 !compact 4th order centered finite difference
 !Header variables
 !f: function to be differentiated
 !df: derivative of f
 implicit none
 integer :: i1,r,c
 integer :: INFO, NHRS, IPIV(N)
 real(kind=8), dimension(:), intent(in) :: f
 real(kind=8), dimension(size(f),1), intent(out) :: df
 real(kind=8), dimension(N,1) :: b,btemp
 real(kind=8), dimension(N,N) :: A ,Atemp
 double precision, allocatable, dimension(:,:) :: x
 
    	
 !formulate matrix b
 b(1,1) = -(dble(5/2))*f(1) + 2.0d0*f(2) + 0.5d0*f(3)
 b(N,1) = -(dble(5/2))*f(N) + 2.0d0*f(N-1) + 0.5d0*f(N-2)
 do i1 = 2,N-1
  b(i1+1,1) = 3.0d0*(f(i1+1)-f(i1-1)) 
 end do 
 b = (dble(1/dx))*b
	
 !formulate matrix A
 A(1,1) = 1.0d0
 A(1,2) = 2.0d0
 A(1,3) = 1.0d0
 A(N,N) = 1.0d0
 A(N,N-1) = 2.0d0
 A(N,N-2) = 1.0d0
 c = 1
 do r = 2,N-1
  A(r, c) = 1.0d0
  A(r,c+1) = 4.0d0
  A(r,c+2) = 1.0d0
  c = c+1
 end do
	
 !use lapack routine to obtain df
 NHRS = 1
 Atemp = A
 btemp = b
		
 call dgesv(N,NHRS, Atemp, N, IPIV, btemp, N, INFO) 
 print *, 'INFO=',INFO
 allocate(x(N,NHRS))
 x = btemp(1:N,:)
 !print *, 'test:',matmul(A,x)-b
 df = x
end subroutine cfd4
!------------------

subroutine test_fd(alpha,error)
    !test accuracy of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
 implicit none
 integer :: i1
 real(kind=8), intent(in) :: alpha
 real(kind=8), intent(out) :: error(2)
 real(kind=8), dimension(N) :: g, f,dy
 real(kind=8) :: df(N-2)
 double precision :: xm, s, sum_i, err1, err2
		
 do i1 = 1,N
 !evaluate 
  xm = dble(dx*(dble(i1)-1.0d0))
  f(i1) = exp(-alpha*(xm - (0.5*N*dx))**2)
  g(i1) = ( -2*alpha*(xm - (0.5*N*dx)) )*f(i1)
 end do
	
 call fd2(f,df)
 s = 0.0d0
 do i1 = 1,N-2
  sum_i = abs(g(i1+1) - df(i1))
  s = s + sum_i
 end do
 err1 = s/(N-2)
 
 s = 0.0d0
 call cfd4(f,dy)
 do i1 = 1,N
  sum_i = abs(g(i1) - dy(i1))
  s = s + sum_i
 end do
 err2 = s/N

 if (abs(err1-err2) > 0.1) then
  s = 0.0d0
  call cfd4(f,dy)
  do i1 = 1,N
   sum_i = abs(g(i1) - dy(i1))
   s = s + sum_i
  end do
  err2 = s/N
 end if 

 error = (/err1,err2/) 

end subroutine test_fd
!---------------------

subroutine test_fd_time(alpha,error,time)
    !test accuracy and speed of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    !time: 2-element array containing wall-clock time required by 1000 calls
    !to fd2 and cfd4
 implicit none
 integer :: i1
 real(kind=8), intent(in) :: alpha
 real(kind=8), intent(out) :: error(2),time(2)
 integer(kind=8) :: clock_t1,clock_t2,clock_rate
 real(kind=8), dimension(N) :: g, f,dy
 real(kind=8) :: df(N-2)
 double precision :: xm, s, sum_i

 do i1 = 1,N 
  xm = dble(dx*(dble(i1)-1.0d0))
  f(i1) = exp(-alpha*(xm - (0.5*N*dx))**2)
  g(i1) = ( -2*alpha*(xm - (0.5*N*dx)) )*f(i1)
 end do

 call system_clock(clock_t1)
 call fd2(f,df)
 call system_clock(clock_t2,clock_rate)
 time(1) = float(clock_t2-clock_t1)/float(clock_rate)
 
 s = 0.0d0
 do i1 = 1,N-2
  sum_i = abs(g(i1+1) - df(i1))
  s = s + sum_i
 end do
 error(1) = dble(s/(N-2))

 call system_clock(clock_t1)
 call cfd4(f,dy)
 call system_clock(clock_t2,clock_rate)
 time(2) = float(clock_t2-clock_t1)/float(clock_rate)
 
 s = 0.0d0
 do i1 = 1,N
  sum_i = abs(g(i1) - dy(i1))
  s = s + sum_i
 end do
 error(2) = dble(s/N)

end subroutine test_fd_time
!---------------------------
end module fdmodule















