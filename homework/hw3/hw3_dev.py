#! /usr/bin/env python
#Assess accuracy and perfomance of fortran finite difference module, fdmodule.
#Converted to python module fd.so using f2py
    
    
from fd import fdmodule as f1 #you may change this line if desired
import numpy as np
import matplotlib.pyplot as plt

def fdtest_a(alpha=100.0):
	"""alpha: input variable which sets width of 
	Guassian function to be differentiated
	""" 
	#Creation of N and dx arrays
	Nt = 7
	s = (1,Nt)
	c = (Nt,2)
	e1 = np.zeros(c)
	d = np.ones(s)
	X = np.ones(s)
	X[0,0] = 100
	m = [0,0]
	
	for i in range(Nt-1): 
		X[0,i+1] = X[0,i]*2
	X[0,5] = 3200
	X[0,6] = 12800
	for i in range(Nt): 
		d[0,i] = 2/(X[0,i]-1)

	#calling of fortran subroutine and filling error arrays
	for i in range(Nt):
		f1.n = X[0,i]
		f1.dx = d[0,i]
		error = f1.test_fd(alpha)
		e1[i,0] = error[0]
		e1[i,1] = error[1]
	
	#Plot figure of error
	plt.figure(1)
	plt.loglog(X[0,:],e1[:,0],'b*')
	plt.loglog(X[0,:],e1[:,1],'r--')
	plt.title(r'Estimate of M, Error vs. N, Paul Mannix, fdtest_a(alpha)')
 	plt.xlabel('N, fd2:blue, cfd4:red')
 	plt.ylabel('Error')
 	plt.grid(True)
	
	
	# Obtain m estimates using polyfit
	p1 = np.polyfit(X[0,:],e1[:,0],1)
	m[0] = p1[0]	
	p2 = np.polyfit(X[0,2:5],e1[2:5,1],1)
	m[1] = p2[0]
	return e1,m  
    
def fdtest_at(alpha=100.0):
	"""alpha: input variable which sets width of 
	Guassian function to be differentiated
	"""
	#Part 3 section 1, calling function for timing
	f1.n = 1000
	f1.dx = 0.002
	ey  = f1.test_fd_time(alpha)
	t1 = ey[0]
	t2 = ey[1]
	print 'fd2 and cfd4 error, N=1000',t1
	print 'fd2 and cfd4 timing, N=1000',t2
	
	#Creation of N and dx arrays
	Nt = 7
	s = (1,Nt)
	c = (Nt,2)
	e2 = np.zeros(c)
	walltime = np.zeros(c) 
	d = np.ones(s)
	X = np.ones(s)
	X[0,0] = 100
	m = [0,0]
	
	#Declare and fill arrays for timesteps
	for i in range(Nt-1): 
		X[0,i+1] = X[0,i]*2
	X[0,5] = 3200
	X[0,6] = 12800
	for i in range(Nt): 
		d[0,i] = 2/(X[0,i]-1)
	for i in range(Nt):
		f1.n = X[0,i]
		f1.dx = d[0,i]
		erry = f1.test_fd_time(alpha)
		# matrices of error and timing
		t1 = erry[0]
		t2 = erry[1]
  		for j in range(2): 
			e2[i,j] = t1[j] 
			walltime[i,j] = t2[j]	

	# Obtain m estimates
	#p1 = np.polyfit(X[0,:],e2[:,0],1)
	#m[0] = p1[0]	
	#p2 = np.polyfit(X[0,2:5],e2[2:5,1],1)
	#m[1] = p2[0]
	#print e2

	plt.figure(2)
	plt.loglog(X[0,:],e2[:,0],'b*')
	plt.loglog(X[0,:],e2[:,1],'r--')
	plt.title(r'Estimate of M, Error vs. N, Paul Mannix, fdtest_at(alpha)')
 	plt.xlabel('N, fd2:blue, cfd4:red')
 	plt.ylabel('Error')
 	plt.grid(True)	
	
	plt.figure(3)
	plt.loglog(X[0,:],walltime[:,0],'b*')
	plt.loglog(X[0,:],walltime[:,1],'r--')
	plt.title(r'Computational time, Walltime vs. N, Paul Mannix, fdtest_at(alpha)')
 	plt.xlabel('N, fd2:blue, cfd4:red')
 	plt.ylabel('Walltime')
 	plt.grid(True)	
	
	return e2, m, walltime 
	

#def fdtest_eff(#set as needed)        
    
	#plt.savefig("hw3.png")
    
#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
	e1,m=fdtest_a(alpha=125.0)
	e2,m,t2 = fdtest_at()
	#add function call to fdtest_eff here if needed
	plt.show()
