!program to test fdmodule in hw3_dev.f90
program test_fdmodule

	use fdmodule
	implicit none
	integer :: X
	real(kind=8) :: alpha
	!alpha = 3.0d0
	real(kind=8) :: error(2)
	
	open(unit=10,file='data.in')
		read(10,*) X
		read(10,*) alpha
	close(10)

	print *, 'Eventually this may work'
	!For this code to work, the user must:
	!1. add variable declarations, alpha (double prec),
	!and error (array of double prec.)
	!2. tell the code where it can find information on dx, N, and test_fd

	
	
	call test_fd(dble(alpha),error)
	!call fd2(f,df)

	print *, 'error=', error
	 
	
	!call fd2(p,df)
	! or:    call test_fd_time(alpha,error,time)
	
    	
	

end program test_fdmodule






