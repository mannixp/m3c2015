#! /usr/bin/env python
# Paul Mannix
# 00688852

#import modules here
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as npr


#---------------------
def test_randn(n,m=0,s=1):
	num_bins = 50
	plt.figure(1)
	plt.hist(s*npr.randn(n,1) + m, num_bins, facecolor='green',alpha=0.5)
	plt.xlabel('npr.randn')
	plt.ylabel('Random Value')
	plt.title(r'Histogram of f(test_randn) (Paul Mannix): $\mu=0$, $\sigma=1$')
	"""Input variables:
	n   number of normal random variables to be generated
	m   random variables generated with mean = m
	s   random variables generated with standard deviation = s
	"""
#----------------------

def wiener1(dt,X0,Nt):

	t = np.linspace(0,100,1000)
	B = np.cumsum(np.sqrt(dt)*(npr.randn(Nt,1)))
	plt.figure(2)
	plt.plot(t, B,'b--')
	plt.title(r'Wiener1, Paul Mannix f(test_randn) with dt=0.1, Nt=1000')
	plt.xlabel('time (t)')
	plt.ylabel('X(t)')
	plt.grid(True)
	"""Input variables:
	dt    time step
	X0    intial value, X(t=0) = X0
	Nt    number of time steps 
	"""
#---------------------
def wienerM(dt,X0,Nt,M):
	
	s = (M,Nt)
	X = np.ones(s)*X0
	for i in range(Nt-1):
		for j in range(M):
			X[j,i+1] = X[j,i] + (np.sqrt(dt))*(npr.randn(1))
 
	mean = np.mean(X,axis=0)
	std = np.std(X,axis = 0)
		
	plt.figure(3)
	t = np.linspace(0,100,1000)

 	meanx= plt.plot(t,mean,'b--')
	plt.plot(t,np.sqrt(t),'r--')
	plt.axhline(y=2,linewidth=2,color='k')
	stdx = plt.plot(t,std,'g--')

	#plt.legend([meanx,stdx],['Mean', 'Std dev'])
 	plt.title(r'WienerM, Paul Mannix, f(wienerm) dt=0.1, X0=2, Nt=1000, M=20000')
 	plt.xlabel('time (t)')
 	plt.ylabel('X(t)')
 	plt.grid(True)

	"""Input variables:
	dt    time step
	X0    intial value, X(t=0) = X0
	Nt    number of time steps 
	M     number of samples
	"""
#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
	t = np.linspace(0,100,1000)
	s = (1000,1)
        X = np.ones(s)*V0
	for i in range(Nt-1):
		X[i+1,0] = X[i,0] - (g*X[i,0]*dt) + (np.sqrt(dt)*(1*npr.randn(1)))	
	
	print "length of X",len(X)
	print "length of t",len(t)
	plt.figure(4)
        plt.plot(t, X,'b--')
        plt.title(r'Langevin1, Paul Mannix f(langevin1) dt=0.1, V0=0.0 Nt=1000, g=0.01')
        plt.xlabel('time (t)')
        plt.ylabel('V(t)')
        plt.grid(True)

	""" Input variables:
	dt    time step
	V0    intial value, V(t=0) = V0
	Nt    number of time steps 
	g     friction factor, gamma
	"""

#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
	s = (M,Nt)
	X = np.ones(s)*V0
	for i in range(Nt-1):
		for j in range(M):
			X[j,i+1] = X[j,i] - (g*X[j,i]*dt) + (np.sqrt(dt)*(1*npr.rand(1)+V0)) 

       	mean = np.mean(X,axis=0)
	#manal(t) = V0*np.exp(-g*t)
	s = (1000,1)
	e = np.ones(s)
	for i in range(Nt):
		e[i] = np.sum((V0*np.exp(-g*(i*dt)))-mean[i])	

	std = np.std(X,axis=0)
	
		
	if debug == True:
		t = np.linspace(0,100,1000)
		plt.figure(5)
		plt.plot(t,(V0*np.exp((-1)*g*t)),'r*')
		plt.plot(t,mean,'k')
		plt.plot(t,((1/(2*g))*(1-np.exp(-2*g*t))),'b*')
		plt.plot(t,np.power(std,2))
		plt.title(r'LangevinM, Paul Mannix f(langevinM) dt=0.1, V0=1.0 Nt=1000,M=2000,g=0.01')
	        plt.xlabel('time (t)')
        	plt.ylabel('V(t)')
        	plt.grid(True)
		plt.hold(True)

		""" Input variables:
		dt    time step
		V0    intial value, V(t=0) = V0
		Nt    number of time steps 
		M     number of samples
		g     friction factor, gamma
		debug flag to control plotting of figures
		"""    
	return (np.sum(e)/Nt)
#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
	"""X = []
	X.append(langevinM(dt,V0,Nt,10,g=1.0,debug=False))
	X.append(langevinM(dt,V0,Nt,100,g=1.0,debug=False))
	X.append(langevinM(dt,V0,Nt,1000,g=1.0,debug=False))#
	X.append(langevinM(dt,V0,Nt,10000,g=1.0,debug=False))

	M = [10,100,1000,10000]
	plt.figure(6)
	plt.loglog(M,X)
	plt.title(r'Log(M) vs. Log(error), Paul Mannix, dt=0.1, V0=1.0 Nt=1000,M=[10,100,1000,10000],g=0.25')
        plt.xlabel('Log(error)')
        plt.ylabel('Log(M)')
        plt.grid(True)
	#return  np.polyfit(np.log(M),np.log(X),1)
	
	Input variables:
	dt    time step
	V0    intial value, V(t=0) = V0
	Nt    number of time steps 
	g     friction factor, gamma
	"""
#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()
